#!/usr/bin/python
'''
Created on Oct 10, 2013

@author: kent

Oct 10, 2013:    Version 1.0
Oct 12, 2013:    Made points bigger, added title to plot
Oct 13, 2013:    Minor bugs fixed
Oct 17, 2013:    Added input argument for selection of map
'''

import sys
import copy
import matplotlib.pyplot as plot

class Map():
    def __init__(self):
        self.tiles = []
        self.walls = []
        self.floor = []
        self.goals = []
        self.diamonds = []
        
class Player():
    def __init__(self, x, y, direction):
        self.x = x
        self.y = y
        self.direction = direction
        
class Field():
    def __init__(self, x, y, ftype):
        self.x = x
        self.y = y
        self.type = ftype

class Diamond():
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Visualizer():
    def __init__(self, mapping, player, levelTitle):
        #    Map
        self.resetLevelData = [copy.deepcopy(mapping), copy.deepcopy(player)]
                        
        #    Plot
        self.vis = plot
        self.fig = self.vis.figure()
        self.fig.canvas.set_window_title('Sokoban game (' + levelTitle + ')')
        self.keyDown = self.fig.canvas.mpl_connect('key_press_event', self.key_press)
          
        self.resetLevel()
        
        self.vis.show()
        
    def key_press(self, event):
        if (event.key == 'escape'):
            print 'Exiting Sokoban...'
            exit()
        else:
            if ((event.key == 'up' or event.key == 'right' or event.key == 'down' or event.key == 'left') and not self.isLevelWon):
                self.rules(event.key)
            if (event.key == 'r'):
                print 'Resetting level ...'
                self.resetLevel()
                               
        self.update()
        
    def resetLevel(self):
        self.mapping = copy.deepcopy(self.resetLevelData[0])
        self.isLevelWon = False
                        
        #    Find limits for plot
        self.minx = -1
        self.maxx = 0
        self.miny = -1
        self.maxy = 0
        
        for i in range(len(self.mapping.tiles)):
            t = self.mapping.tiles[i]
            
            if (t.x > self.maxx):
                self.maxx = t.x
            if (t.y > self.maxy):
                self.maxy = t.y
                
        self.maxx += 1
        self.maxy += 1
        
        #    Player
        self.playerMoves = 0
        self.player = copy.deepcopy(self.resetLevelData[1])
        
        #    Update graphics
        self.update()
        
    def rules(self, keyEvent):
        if (keyEvent == self.player.direction):
            x = 0
            y = 0
            
            if (keyEvent == 'up'): y -= 1
            if (keyEvent == 'down'): y += 1
            if (keyEvent == 'left'): x -= 1
            if (keyEvent == 'right'): x += 1
            
            xTempPlayer = self.player.x + x
            yTempPlayer = self.player.y + y
                        
            #    Check for collision with walls
            isPlayerWallColliding = False
            for wall in self.mapping.walls:
                if (xTempPlayer == wall.x and yTempPlayer == wall.y):
                    isPlayerWallColliding = True
                    break
                
            if (not isPlayerWallColliding):
                #    Check if player is colliding with any diamond on the map
                currentDiamond = 0
                isCollisionDetected = False
                isPlayerDiamondColliding = False
                
                for diamond in self.mapping.diamonds:
                    if (xTempPlayer == diamond.x and yTempPlayer == diamond.y):
                        isPlayerDiamondColliding = True
                        currentDiamond = diamond
                        
                        #    Check if current diamond if colliding with other diamonds/walls
                        for e in (self.mapping.diamonds + self.mapping.walls):
                            if (diamond.x + x == e.x and diamond.y + y == e.y):
                                isCollisionDetected = True
                                break
                                
                        break
                    
                if (not isCollisionDetected):
                    #    Move player
                    self.player.x = xTempPlayer
                    self.player.y = yTempPlayer
                    self.playerMoves += 1
                    
                    #    Print moves 
                    if (self.playerMoves % 10 == 0):
                        print 'Moves: ' + str(self.playerMoves)
                    
                    if (isPlayerDiamondColliding):
                        #    Move jewel
                        currentDiamond.x += x
                        currentDiamond.y += y
        else:
            self.player.direction = keyEvent
            
        #    Check if game is won
        numberOfDiamondsReachedGoal = 0
        numberOfDiamonds = len(self.mapping.diamonds)
        
        for diamond in self.mapping.diamonds:
            for goal in self.mapping.goals:
                if (diamond.x == goal.x and diamond.y == goal.y):
                    numberOfDiamondsReachedGoal += 1
                    
        if (numberOfDiamondsReachedGoal == numberOfDiamonds):
            self.isLevelWon = True
            print 'Congratulations, game WON!'
            print 'Total moves: ' + str(self.playerMoves)
            
    def update(self):
        #    Plot settings
        self.vis.clf()
        self.vis.xlim(self.minx,self.maxx)
        self.vis.ylim(self.miny,self.maxy)
        self.vis.gca().set_xticks(range(self.minx,self.maxx-self.minx))
        self.vis.gca().set_yticks(range(self.miny,self.maxy-self.miny))
        self.vis.gca().invert_yaxis()
        self.vis.grid(True)
        
        markerSize = 30.0	#  TODO: Make this dynamic
        
        #    Plot map
        for t in self.mapping.tiles:
            if (t.type == 'WALL'):
                plot.plot(t.x, t.y, 'ko', ms=markerSize)
            if (t.type == 'FLOOR'):
                plot.plot(t.x, t.y, 'wo', ms=markerSize)
            if (t.type == 'GOAL'):
                plot.plot(t.x, t.y, 'go', ms=markerSize)
                
        #    Plot diamonds
        for d in self.mapping.diamonds: self.vis.plot(d.x, d.y, 'mo', ms=markerSize)
        
        #    Plot player and direction
        if (self.player.direction == 'up'):
            xDir = self.player.x
            yDir = self.player.y - 0.5
        if (self.player.direction == 'down'):
            xDir = self.player.x
            yDir = self.player.y + 0.5
        if (self.player.direction == 'left'):
            xDir = self.player.x - 0.5
            yDir = self.player.y
        if (self.player.direction == 'right'):
            xDir = self.player.x + 0.5
            yDir = self.player.y
        
        self.vis.plot(self.player.x, self.player.y, 'ro', ms=markerSize)
        self.vis.plot([self.player.x, xDir], [self.player.y, yDir], 'b', lw=3.0)
        
        #    Draw plot
        self.vis.draw()

def mapFileParser(mapFile):
    mapping = Map()
    player = Player(0,0,'up')
    levelTitle = ''
    line = 0
        
    x = 0
    y = -1
            
    with open(mapFile, 'rb') as fileDescriptor:
        byte = True
        
        while byte:
            field = 0
            byte = fileDescriptor.read(1)
            
            if (line == 0 and not byte == '\n'):
                levelTitle += str(byte)
                        
            if (byte == '\n'):
                x = 0
                y += 1
                line += 1
            else:
                if (byte.lower() == 'x'):
                    field = Field(x,y,'WALL')   #    WALL
                    mapping.walls.append(field)
                if (byte.lower() == 'g'):
                    field = Field(x,y,'GOAL')   #    GOAL
                    mapping.goals.append(field)
                if (byte.lower() == '.' or byte.lower() == 'j' or byte.lower() == 'm'):
                    field = Field(x,y,'FLOOR')  #    FLOOR
                    mapping.floor.append(field)
                    if (byte.lower() == 'j'):  #    DIAMOND
                        diamond = Diamond(x,y)
                        mapping.diamonds.append(diamond)
                    if (byte.lower() == 'm'):  #    PLAYER
                        player.x = x
                        player.y = y
                
                if (field):
                    mapping.tiles.append(field)
                    
                x += 1
                
    return [mapping, player, levelTitle]

if __name__ == '__main__':
    print 'Starting Sokoban ...'
    
    if (len(sys.argv) == 2):
        mapfile = sys.argv[1]
    else:
        print 'Error: One argument needed for the map-file'
        print './sokobanGame.py map-file'
    
    #    Load file and parse
    data = mapFileParser(mapfile)
    
    #    Visualization
    vis = Visualizer(data[0], data[1], data[2])
        
    #   Loop forever
    while True: dummy = 1
