#!/usr/bin/env python
import collections
import copy

class MoveException(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

class Map:
	TileType = collections.namedtuple('TileType',('OPEN','BRICK','GOAL','JEWEL','PLAYER'))(0,1,2,3,4)
	chr2type={'.':TileType.OPEN,'X':TileType.BRICK,'G':TileType.GOAL,'J':TileType.JEWEL,'M':TileType.PLAYER}
	def __init__(self, filename):
		F = open(filename,'r')
		header = F.readline().strip().split(' ')
		self.data = {}
		self.data['width'] = int(header[0])
		self.data['height'] = int(header[1])
		self.data['numJewels'] = int(header[2])
		self.data['jewels'] = []
		self.data['goals'] = []
		self.data['player'] = (0,0)
		self.data['map'] = []
		for y in range(self.data['height']):
			self.data['map'].append( [Map.TileType.OPEN for x in range(self.data['width'])] )
		y = 0
		for row in F:
			row=row.strip()
			x=0
			for char in row:
				self.setTile(x,y,char)
				x+=1
			y+=1
		self.orig = copy.deepcopy(self.data) 
		F.close()
	def resetToOrig(self):
		self.data = copy.deepcopy(self.orig)
	def getTile(self,x,y):
		return self.data['map'][y][x]
	def isValidMove(self,x,y,dx,dy):
		if (x<0 or x >self.width) or (y<0 or y>self.height):
			return False 
		currentTile = self.getTile(x,y)
		if currentTile != Map.TileType.JEWEL or currentTile != Map.TileType.PLAYER:
			return False
		newTile = self.getTile(x+dx,y+dy)
		if newTile != Map.TileType.OPEN:
			return False
		return True
	def move(self,x,y,dx,dy):
		currentTile = self.getTile(x,y)
		if currentTile == Map.TileType.JEWEL:
			self.data['jewels'].remove((x,y))
			self.setTile(x,y,Map.TileType.OPEN)
			self.data['jewels'].append((x+dx,y+dy))
			self.setTile(x+dx,y+dx,Map.TileType.JEWEL)
		elif currentTile == Map.TileType.PLAYER:
			self.data['player'] = (x+dx,y+dy)
			self.setTile(x,y,Map.TileType.OPEN)
			self.setTile(x+dx,y+dy,Map.TileType.PLAYER)
		else:
			raise MoveException(currentTile)
	def setTile(self, x, y, newTile):
		if type(newTile)==type('0'):
			newTile = Map.chr2type[newTile]
			
		self.data['map'][y][x] = newTile
		
		if  newTile == Map.TileType.GOAL:
			self.data['goals'].append((x,y))
		elif newTile == Map.TileType.JEWEL:
			self.data['jewels'].append((x,y))
		elif newTile == Map.TileType.PLAYER:
			self.data['player'] = (x,y)
	def pprint(self):
		for row in self.data['map']:
			for col in row:
				print str(col),
			print "\n",
		print "Goals:"
		for goal in self.data['goals']:
			print(goal[0],goal[1])
		print "Jewels:"
		for jewel in self.data['jewels']:
			print(jewel[0],jewel[1])
