#!/usr/bin/env python
class SokoSolver(object):
	def __init__(self, _mapObj):
		self.mapObj = _mapObj
	def process(self):
		self.mapObj.pprint()

class DFSolver(SokoSolver):
	def __init__(self, _mapObj):
		super(DFSolver,self).__init__(_mapObj)

from util import Map
if __name__ == "__main__":
	mapObj = Map('./map.txt')
	solver = DFSolver(mapObj)
	solver.process()
